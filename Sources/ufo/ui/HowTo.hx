package ufo.ui;

import koui.elements.*;
import koui.elements.layouts.*;
import koui.elements.layouts.Layout.Anchor;

class HowTo {
	public static var timer = 0;

	static var howTo: Array<String> = [
		"", // Separator
		"You are the pilot of a UFO of very hungry aliens. Your goal is to steal",
		"all the cows without getting caught by the humans.",
		"",
		"Humans will spot you if you fly too close to houses or to cars or when",
		"the sun rises (6:00 AM). The time is displayed at the top right of the screen.",
		"",
		"- People in HOUSES are asleep and it takes a while until you are spotted.",
		"- People in CARS will see you much faster, so be aware of them!",
		"",
		"If you turn off the UFO's light beam, you can't be seen from humans",
		"but without the light beam you won't be able to steal cows.",
		"",
		"The key bindings are shown at the bottom of the screen when playing.",
		"",
	];

	public static function setup(layout: AnchorPane) {
		var wrapper = new AnchorPane(0, 0, 800, 600);
		wrapper.setTID("lyt_fixed");
		var expander = new Expander(0, 0, 800, 0, DOWN);
		expander.spacing = 2;

		var title = new Label("How To Play");
		title.alignmentHor = TextCenter;
		title.posX = 400;

		expander.add(title);

		// Fake multi-line label
		for (entry in howTo) {
			var label = new Label(entry);
			label.setTID("label_paragraph");
			label.alignmentHor = TextCenter;
			label.posX = 400;
			expander.add(label);
		}

		var btnMainMenu = UIMain.makeButton("Main Menu", () -> {
			UIMain.setScene("MainMenu");
		});

		var btnPlay = UIMain.makeButton("Play", () -> {
			UIMain.setScene("Level");

			var musicChannel = Sound.channels["MainMenu"];
			if (musicChannel != null) {
				musicChannel.stop();
				Sound.channels.remove("MainMenu");
			}
		});

		var buttonBar = new ColLayout(0, 0, 0, 80, 2);

		buttonBar.addToColumn(btnMainMenu, 0);
		buttonBar.addToColumn(btnPlay, 1);

		expander.add(buttonBar);

		wrapper.add(expander, MiddleCenter);
		layout.add(wrapper, MiddleCenter);
	}

	public static function update() {
		var music = Sound.channels["MainMenu"];
		music.volume = Math.max(0, music.volume - 0.5 * (iron.system.Tween.easeCubicInOut(timer * iron.system.Time.realDelta / 4)));
		timer++;
	}
}
