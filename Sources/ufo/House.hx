package ufo;

import iron.object.Object;

using armory.object.TransformExtension;

class House extends Observer {

	public static var houseLightsOn: Array<Int> = new Array(); // 0 or 1

	public function new() {
		super();

		notifyOnInit(function() {
			houseLightsOn.push(0);
			object.properties = ["houseIndex" => houseLightsOn.length - 1];

			alertTime = 3 * 60;
			bustedTime = 6 * 60;
			radius = 20;
		});
	}

	public override function alertCancelled() {
		var houseIndex = object.properties.get("houseIndex");
		Sound.playSound("Switch", 0.7);
		houseLightsOn[houseIndex] = 0;
	}

	public override function alertBegin() {
		var houseIndex = object.properties.get("houseIndex");
		Sound.playSound("Switch", 0.7);
		houseLightsOn[houseIndex] = 1;
	}

	public static function intLink(object: Object, mat: iron.data.MaterialData, link: String): Null<Int> {
		if (link != "lightsOn") return null;
		if (object.properties == null) return 0;

		var i: Null<Int> = object.properties.get("houseIndex");
		if (i == null) return 0;

		return (i < houseLightsOn.length) ? houseLightsOn[i] : 0;
	}

	public static function reset() {
		houseLightsOn.resize(0);
	}
}
