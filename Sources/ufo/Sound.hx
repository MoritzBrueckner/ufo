package ufo;

import ufo.audio.UFO_Audio;
import ufo.audio.UFO_ResamplingAudioChannel;

using StringTools;

class Sound extends iron.Trait {
	// 44100 * (44100 / 48000), Armorcore seems fill the buffer too fast
	public static inline var SAMPLE_RATE = 40516;

	public static var masterVolume = 0.5;
	public static var channels: Map<String, UFO_ResamplingAudioChannel> = new Map();

	public static var playingInScene: Array<String> = new Array();

	public function new() {
		super();

		notifyOnInit(function() {
			switch(iron.Scene.active.raw.name) {
				case "Level":
					playInScene("NightBG", true);
					playInScene("MusicLevel", false, 0.4);
					playInScene("UfoAccel", true, 0.86);
					playInScene("UfoEngine", true, 0.45);
				case "HowTo" | "MainMenu":
					if (channels["MainMenu"] == null) {
						playSound("MainMenu", true, 0.55);
					}
					if (iron.Scene.active.raw.name == "HowTo") {
						playInScene("NightBG", true);
					}
				default:
			}
		});
	}

	public static function playSound(assetname: String, loop = false, volume = 1.0, sampleRate = SAMPLE_RATE, inScene = false) {
		var sound = kha.Assets.sounds.get(assetname);
		if (sound == null) {
			trace('Could not load sound $assetname');
			return;
		}

		sound.sampleRate = sampleRate;
		var channel = UFO_Audio.play(sound, loop);
		if (channel == null) return;
		channel.volume = volume;
		channels[assetname] = channel;
		if (inScene) playingInScene.push(assetname);
	}

	public static inline function playInScene(assetname: String, loop = false, volume = 1.0, sampleRate = SAMPLE_RATE) {
		playSound(assetname, loop, volume, sampleRate, true);
	}

	public static function interpolateVolume(assetname: String, targetVol: Float, factor: Float) {
		var channel: Null<UFO_ResamplingAudioChannel> = channels[assetname];
		if (channel == null) return;

		var diff = targetVol - channel.volume;
		if (Math.abs(diff) > 0.01) channel.volume += diff * factor;
	}

	public static function interpolatePitch(assetname: String, targetPitch: Float, factor: Float) {
		var channel: Null<UFO_ResamplingAudioChannel> = cast channels[assetname];
		if (channel == null) return;

		var diff = targetPitch - channel.pitch;
		if (Math.abs(diff) > 0.01) {
			channel.pitch += diff * factor;
		}
		else {
			channel.pitch = targetPitch;
		}
	}

	public static function clearScene() {
		for (name in playingInScene) {
			if (channels[name] != null) {
				channels[name].stop();
			}
		}
		playingInScene.resize(0);
	}
}
