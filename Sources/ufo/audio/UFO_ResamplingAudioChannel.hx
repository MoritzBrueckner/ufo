// =============================================================================
// See https://github.com/Kode/Kha/blob/master/Sources/kha/audio2/ResamplingAudioChannel.hx
// =============================================================================

package ufo.audio;

import kha.arrays.Float32Array;
import kha.audio2.ResamplingAudioChannel;


class UFO_ResamplingAudioChannel extends ResamplingAudioChannel {
	public var pitch: Float = 1.0;
	public var floatPosition: Float = 0.0;

	// Lowpass filter
	public var buf0 = 0.0;
	public var buf1 = 0.0;
	public var cutoff = 1.0;

	public function new(looping: Bool, sampleRate: Int) {
		super(looping, sampleRate);
	};

	override public function nextSamples(requestedSamples: Float32Array, requestedLength: Int, sampleRate: Int): Void {
		if (paused || stopped) {
			for (i in 0...requestedLength) {
				requestedSamples[i] = 0;
			}
			return;
		}

		var requestedSamplesIndex = 0;
		while (requestedSamplesIndex < requestedLength) {
			for (i in 0...ResamplingAudioChannel.min(sampleLength(sampleRate) - myPosition, requestedLength - requestedSamplesIndex)) {
				// Make sure that we store the actual float position
				floatPosition += pitch;
				myPosition = Std.int(floatPosition);

				var sampledVal = sampleFloatPos(floatPosition, i % 2 == 0, sampleRate);
				// sampledVal = lowPassFilter(sampledVal)

				requestedSamples[requestedSamplesIndex++] = sampledVal;
			}

			if (floatPosition >= sampleLength(sampleRate)) {
				myPosition = 0;
				floatPosition = floatPosition % 1; // Keep fraction
				if (!looping) {
					stopped = true;
					break;
				}
			}
		}

		while (requestedSamplesIndex < requestedLength) {
			requestedSamples[requestedSamplesIndex++] = 0;
		}
	}

	inline function sampleFloatPos(position: Float, even: Bool, sampleRate: Int): Float {
		// Like super.sample(), just with position: Float for correct
		// interpolation of float positions for pitch shifting

		// Also replaced 'even' to correct the stereo output (buffer is interleaved)
		// var even = position % 2 == 0;

		// TODO: Locate clicks (could be an issue with the sound file itself)

		var factor = this.sampleRate / sampleRate;

		position = Std.int(position / 2);
		var pos = factor * position;
		var pos1 = Math.floor(pos);
		var pos2 = Math.floor(pos + 1);
		pos1 *= 2;
		pos2 *= 2;

		var minimum: Int;
		var maximum: Int;

		if (even) {
			minimum = 0;
			maximum = data.length - 1;
			maximum = maximum % 2 == 0 ? maximum : maximum - 1;
		}
		else {
			pos1 += 1;
			pos2 += 1;

			minimum = 1;
			maximum = data.length - 1;
			maximum = maximum % 2 != 0 ? maximum : maximum - 1;
		}

		var a = (pos1 < minimum || pos1 > maximum) ? 0 : data[pos1];
		var b = (pos2 < minimum || pos2 > maximum) ? 0 : data[pos2];
		a = (pos1 > maximum) ? data[maximum] : a;
		b = (pos2 > maximum) ? data[maximum] : b;
		return lerp(a, b, pos - Math.floor(pos));
	}

	override public function stop() {
		super.stop();
		floatPosition = 0.0;
	}

	override public function pause() {
		super.pause();
		floatPosition = myPosition;
	}

	function lowPassFilter(value: Float) {
		// http://www.martin-finke.de/blog/articles/audio-plugins-013-filter/
		buf0 += cutoff * (value - buf0);
		buf1 += cutoff * (buf0 - buf1);
		return buf1;
	}
}
