package ufo;

import iron.math.Vec4;

using armory.object.TransformExtension;

class Car extends Observer {
	static inline var CAR_SPEED = 0.2;

	public function new() {
		super();

		notifyOnInit(() -> {
			// Only use cars in collection instances
			if (object.parent == null) return;

			alertTime = 1 * 60;
			bustedTime = Std.int(1.8 * 60);
			radius = 30;
		});

		notifyOnUpdate(() -> {
			var pos = object.transform.loc;
			pos.y -= CAR_SPEED;

			// "Respawn" at the beginning, use local position here in order to
			// not have to use a direction parameter
			if (pos.y <= -280) {
				pos.y = 0;
			}
			object.transform.buildMatrix();

			// Update world coordinates for observer system
			pos = object.transform.getWorldPosition();
			position.x = pos.x;
			position.y = pos.y;
		});
	}
}
