package ufo.ui;

import koui.elements.*;
import koui.elements.layouts.*;
import koui.elements.layouts.Layout.Anchor;

class Intro {
	static var initialized = false;
	static var timer = 0;

	static var mbLogo: ImagePanel;
	static var armoryLogo: ImagePanel;
	static var textPowered: Label;
	static var textArmory3D: Label;
	static var layout: AnchorPane;

	public static function setup(layout: AnchorPane) {
		Intro.layout = layout;

		var imgMB = kha.Assets.images.get("MBLogo");
		var imgArm = kha.Assets.images.get("ArmoryLogo");

		mbLogo = new ImagePanel(imgMB);
		mbLogo.setScaleQuality(High);
		mbLogo.setScale(true, 400, 400);

		armoryLogo = new ImagePanel(imgArm);
		armoryLogo.setScaleQuality(High);
		armoryLogo.setScale(true, 400, 400);

		textPowered = new Label("Powered by");
		textPowered.posY -= Std.int(imgArm.realHeight / 2.0);
		textPowered.setTID("label_intro");
		textPowered.alignmentHor = TextCenter;
		textPowered.alignmentVert = TextBottom;
		textArmory3D = new Label("Armory3D");
		textArmory3D.posY += Std.int(imgArm.realHeight / 2.0);
		textArmory3D.setTID("label_intro");
		textArmory3D.alignmentHor = TextCenter;
		textArmory3D.alignmentVert = TextTop;

		armoryLogo.visible = false;
		textPowered.visible = false;
		textArmory3D.visible = false;

		layout.add(mbLogo, Anchor.MiddleCenter);
		layout.add(armoryLogo, Anchor.MiddleCenter);
		layout.add(textPowered, Anchor.MiddleCenter);
		layout.add(textArmory3D, Anchor.MiddleCenter);

		initialized = true;
	}

	@:access(koui.elements.Element)
	public static function update() {
		if (!initialized) return;

		// Hack as long as there is no animation support in Koui
		// Wave function for blending in/out that is capped at 1 for some visible time
		// See https://en.wikipedia.org/wiki/Triangle_wave#Modulo_operation
		var period = 4;
		var blendTime = 1;
		var opacity = 1 - Math.min(1 / blendTime * Math.abs((((timer / 60) - period / 2) % period) - period / 2), 1);
		UIMain.setBlendOpacity(opacity);

		if (timer >= 60 * period) {
			armoryLogo.visible = true;
			textPowered.visible = true;
			textArmory3D.visible = true;
			mbLogo.visible = false;

			if (timer >= 60 * period * 2) {
				UIMain.setBlendOpacity(0);
				UIMain.setScene("MainMenu");
			}
		}

		timer++;
	}
}
