package ufo.ui;

import kha.FastFloat;

import koui.elements.*;
import koui.elements.layouts.*;
import koui.events.Events.Event;

using StringTools;

class Level {
	static var initialized = false;

	static var scoreText: Label;
	static var timeText: Label;
	static var alertLevel: Progressbar;
	public static var visualGuides: Checkbox;

	public static function setup(layout: AnchorPane) {
		layout.setPadding(20, 20, 20, 20);
		scoreText = new Label("");
		scoreText.setTID("label_hud");

		alertLevel = new Progressbar(0.0, 1.0);
		alertLevel.label = "Alert Level";
		alertLevel.width = 512;

		timeText = new Label("");
		timeText.alignmentHor = TextRight;
		timeText.setTID("label_hud");

		var helpText = new Label("W/A/S/D: Move UFO   |   Shift: Fly faster   |   Spacebar: Toggle light beam");
		helpText.alignmentVert = TextBottom;
		helpText.setTID("label_paragraph");

		visualGuides = new Checkbox("Show Observer Range On/Off");
		visualGuides.height = 24;
		visualGuides.width = 340; // Hard coded for now, find good solution for Koui
		visualGuides.posY += 6;
		visualGuides.onMouseClick((e: Event) -> {
			if (e.state == Deactivated) {
				setVisualGuides(visualGuides.isChecked);
			}
		});

		layout.add(scoreText);
		layout.add(alertLevel, TopCenter);
		layout.add(timeText, TopRight);
		layout.add(helpText, BottomLeft);
		layout.add(visualGuides, BottomRight);

		setScore(0);
		setTime(0.0);
	}

	@:access(koui.elements.Element)
	public static function update() {
		if (!initialized) return;
	}

	public static function setScore(score: Int) {
		scoreText.text = 'Cows: $score/${ufo.Cow.amount}';
	}

	public static function setTime(time: Float) {
		var minutes = Std.int((time % 1.0) * 60);
		var minutesStr = minutes < 10 ? '0$minutes': Std.string(minutes);

		timeText.text = '0${Std.int(time)}:$minutesStr AM';
	}

	public static function setAlertLevel(level: FastFloat) {
		alertLevel.value = level;
	}

	public static inline function showGameOver() {
		UIMain.showDialog("Game Over!", "The humans were alarmed!", "Main Menu", () -> {
			UIMain.setScene("MainMenu");
		});
	}

	public static inline function showWin() {
		UIMain.showDialog("You won!", "No humans were alarmed and all cows were stolen!", "Main Menu", () -> {
			UIMain.setScene("MainMenu");
		});
	}

	public static function setVisualGuides(val: Bool) {
		for (obj in iron.Scene.active.meshes) {
			if (obj.name.startsWith("RadiusIndicator")) {
				obj.visible = val;
			}
		}
	}
}
