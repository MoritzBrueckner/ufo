package ufo.geom;

import kha.FastFloat;

import iron.math.Vec4;

import armory.math.Helper;

/** Simple cone with infinite height. **/
class Cone {
	public var origin: Vec4;
	public var direction: Vec4;
	public var angleDeg: FastFloat;

	public inline function new(origin: Vec4, direction: Vec4, angleDeg: FastFloat) {
		this.origin = origin;
		this.direction = direction;
		this.angleDeg = angleDeg;
	}

	/**
		Checks if a goven point is inside the cone.
		@return -1, if the point is outside, else the relative distance of the
				point to the cone's center.
	**/
	public function pointInside(p: Vec4): Float {
		// https://stackoverflow.com/a/12826333/9985959

		var relPos = p.clone().sub(origin);
		var coneDist = relPos.dot(direction);
		var radius = Math.sin(Helper.degToRad(angleDeg)) * coneDist;
		var orthDist = relPos.sub(direction.clone().mult(coneDist)).length();

		return (orthDist < radius) ? (orthDist / radius) : -1;
	}
}
