// =============================================================================
// See https://github.com/Kode/Kha/blob/master/Sources/kha/audio2/Audio1.hx
// =============================================================================

package ufo.audio;

#if cpp
import sys.thread.Mutex;
#end

import kha.audio2.Audio1;

@:access(kha.audio2.Audio1)
class UFO_Audio {
	public static function play(sound: kha.Sound, loop: Bool = false): UFO_ResamplingAudioChannel {
		var channel = new UFO_ResamplingAudioChannel(loop, sound.sampleRate);
		channel.data = sound.uncompressedData;
		var foundChannel = false;

		#if cpp
		mutex.acquire();
		#end
		for (i in 0...Audio1.channelCount) {
			if (Audio1.soundChannels[i] == null || Audio1.soundChannels[i].finished) {
				Audio1.soundChannels[i] = channel;
				foundChannel = true;
				break;
			}
		}
		#if cpp
		mutex.release();
		#end

		return foundChannel ? channel : null;
	}
}
