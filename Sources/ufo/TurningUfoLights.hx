package ufo;

import iron.system.Time;

class TurningUfoLights extends iron.Trait {

	static inline var speed = 2.0;

	static inline var steps = 4;
	static var angle = Math.PI / 16.0;

	public function new() {
		super();

		notifyOnInit(function() {
		});

		notifyOnUpdate(function() {
			var i = Std.int(Time.time() * 2) % steps;

			object.transform.setRotation(0, 0, i * angle);
			object.transform.buildMatrix();
		});

		// notifyOnRemove(function() {
		// });
	}
}
