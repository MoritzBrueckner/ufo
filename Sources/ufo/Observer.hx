package ufo;

import kha.FastFloat;

import iron.math.Vec2;
import iron.object.Object;

using armory.object.TransformExtension;

// =============================================================================
//
//         WARNING: very ugly code ahead!
//
// =============================================================================

class Observer extends iron.Trait {
	public static var observers: Array<Observer> = new Array();

	public static var stopUpdate = false;

	static var maxAlertLevel: FastFloat = 0.0;
	static var isUfoVisible = false;

	var alertTime = 3 * 60;
	var bustedTime = 6 * 60;
	var radius = 20;

	var timer = 0;
	var alerted = false;
	var position: Vec2;

	static var initialized = false;

	static var removals: Array<Int> = new Array();

	public function new() {
		super();

		notifyOnInit(function() {
			if (!initialized) {
				iron.object.Uniforms.externalIntLinks.push(House.intLink);
				initialized = true;
			}

			observers.push(this);

			var pos = object.transform.getWorldPosition();
			position = new Vec2(pos.x, pos.y);
		});
	}

	public function alertCancelled() {}
	public function alertBegin() {}

	inline function update(): FastFloat {
		if (timer > bustedTime) timer = bustedTime;

		timer--;
		if (timer <= 0) {
			timer = 0;
			if (alerted) {
				alertCancelled();
				alerted = false;
			}
		}

		// Relative alert level
		return timer / bustedTime;
	}

	function updateAlerts(beamPos: Vec2): Bool {
		var dst = Vec2.distance(position, beamPos);

		if (dst < radius) {
			timer += 2; // 2 because it is already decremented by 1 every frame
			isUfoVisible = true;

			// Alerted
			if (timer > alertTime) {
				if (!alerted) alertBegin();
				alerted = true;

				// Detected
				if (timer > bustedTime) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Called every frame. Returns whether the ufo was detected (game over).
	 */
	public static function updateAll(beamOn: Bool, beamPos: Vec2): Bool {
		if (stopUpdate) return false;

		maxAlertLevel = 0.0;
		for (observer in observers) {
			var alertLevel: FastFloat = inline observer.update();
			if (alertLevel > maxAlertLevel) maxAlertLevel = alertLevel;
		}
		ufo.ui.Level.setAlertLevel(maxAlertLevel);

		isUfoVisible = false;

		if (beamOn) {
			for (observer in observers) {
				if (observer.updateAlerts(beamPos)) return true;
			}
		}

		if (!isUfoVisible && maxAlertLevel > 0.95) {
			ufo.ui.Achievements.showAchievement("As close as you can get");
		}
		return false;
	}


	public static function reset() {
		stopUpdate = false;
		observers.resize(0);
		iron.object.Uniforms.externalIntLinks.remove(House.intLink);

		House.reset();

		initialized = false;
	}
}
