# Project files for "The Grand Cattle Heist"

![](https://img.itch.zone/aW1nLzU3MzEzNjIucG5n/original/rc02h3.png)

This is the repository for my game [The Grand Cattle Heist](https://timodriaan.itch.io/the-grand-cattle-heist) that I made for the first [Armory 3D Community Game Jam](https://github.com/armory3d/armory/wiki/Games-made-with-Armory#armory-3d-community-game-jam-entries) in spring 2021. The game was made in a short time, so expect some chaos and unused stuff in the project files :)

You might need to enable Git LFS in your local clone of this repository in order to use all the assets. Also, you might need to replace `Subprojects/Koui` with a clone from [here](https://gitlab.com/koui/Koui).

The project was made with Armory SDK 2021.04, but I made some minor changes to this repo to make it compile on Armory SDK 2021.12. If you want the original April version, please check out the repository on commit [`101019f7`](https://gitlab.com/MoritzBrueckner/ufo/-/commit/101019f72f49372c59b7873dd9d180b4b674a400).

Due to the updates in Armory in the meantime, there are some differences when you play the game with a newer SDK (e.g. 2021.12). The ones I know are:
- the scene is brighter
- there are no shadows for the light beam (?)
- the background fog in the menu is freaking out for some reason

In my local version of the game, I made some changes to the Koui submodule (user interface library) which are uncommitted and thus not included in this repository. The game won't break because of that, but you will not see a pointing cursor when hovering over links in the "About" section for example.
