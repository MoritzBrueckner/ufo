package ufo;

import kha.arrays.Float32Array;

import iron.Scene;
import iron.data.SceneFormat;
import iron.math.Mat4;
import iron.math.Vec2;
import iron.math.Vec4;
import iron.math.Quat;
import iron.object.Object;
import iron.object.LightObject;
import iron.system.Input;
import iron.system.Time;
import iron.system.Tween;

import armory.math.Helper;
import armory.trait.physics.RigidBody;

import ufo.Sound;
import ufo.geom.Cone;

using armory.object.TransformExtension;


/**
- Collision: Licht flackern lassen am Ufo, aber nur wenn schnell genug

- Ahcievment für Last Second rettung? Also wenn 98% alarm oder so und dann schaffst du es weg

Gute Sounds:
- Weapon Shot Spaceship Super Blaster
- SF-sdb_WHOOSH-15_space...
- Spaces - Generator_08
- AbstractSpaceship...
- spacecraft engine 2.wav
- Delay_Movement_Surreal-01.wav
- Pick ups für mehr beam strength zb?
**/

class Ufo extends iron.Trait {

	static inline var SPEED = 10;
	static inline var RUN_FACTOR = 1.8;
	static inline var TILT_ANGLE = 10;
	static inline var TILT_SMOOTHNESS = 0.1;
	static inline var BEAM_STRENGTH = 29;

	public static var instance: Ufo;

	// Front back left right
	@prop var boundsFBLR: Vec4;

	// Do not steer the UFO in the main menu
	var inLevel = false;
	// Pause
	var playable = false;

	var rigidBody: RigidBody;
	var camera: Object;
	var camPos: Object;
	var mesh: Object;
	var ambientLightRaw: TLightData;
	static var ambientLightColor: Null<Float32Array>;
	static var ambientLightStrength: Float;
	static var beamColor: Float32Array;
	static inline var beamLightStrength: Float = 35.0;

	var kb: Keyboard;
	var direction: Vec4;
	var tilting: Vec4;

	var camDist: Vec4;
	var height: kha.FastFloat;

	public var beamOn(default, set) = true;
	public var beamCone: Cone;

	var score = 0;

	var collidedLastFrame = false;
	// When a collision occurs, the object is already "stopped"
	var lastVelocity: Vec4 = new Vec4();

	public function new() {
		super();

		instance = this;

		notifyOnInit(onInit);
		notifyOnUpdate(onUpdate);
	}

	function onInit() {
		inLevel = playable = iron.Scene.active.raw.name == "Level";
		mesh = Scene.active.getMesh("UfoMesh");

		rigidBody = object.getTrait(RigidBody);
		rigidBody.disableGravity();
		rigidBody.activate();

		// Seemingly, the gravity is calculated oncy before this trait is initialized,
		// so make sure to set the ufo back to the correct height
		object.transform.loc.z = 18;
		object.transform.buildMatrix();

		height = object.transform.loc.z;

		if (!inLevel) return;

		direction = new Vec4();
		tilting = new Vec4();
		kb = Input.getKeyboard();

		ufo.ui.Level.setScore(score);

		ambientLightRaw = Scene.active.getLight("UfoAmbientLight").data.raw;

		// Static initialization. Make sure that we don't initialize the color
		// from the recently turned on beam when restarting the scene.
		if (ambientLightColor == null) {
			ambientLightColor = ambientLightRaw.color;
			ambientLightStrength = ambientLightRaw.strength;

			beamColor = new Float32Array(4);
			beamColor[0] = 0.098;
			beamColor[1] = 0.943;
			beamColor[2] = 1.0;
			beamColor[3] = 1.0;
		}

		ambientLightRaw.color = beamOn ? beamColor : ambientLightColor;
		ambientLightRaw.strength = beamOn ? beamLightStrength : ambientLightStrength;

		camera = Scene.active.getCamera("Camera");
		camPos = Scene.active.getEmpty("CamPos");

		camDist = camPos.transform.loc.clone().sub(object.transform.loc);

		beamCone = new Cone(new Vec4(), new Vec4(), 12); // 14 degrees measured in Blender

		iron.Scene.active.notifyOnInit(() -> {
			ufo.ui.Level.setVisualGuides(ufo.ui.Level.visualGuides.isChecked);
		});
	}

	function onUpdate() {
		mesh.transform.rotate(Vec4.zAxis(), 0.004);
		var heightFactor = inLevel ? 0.25 : 2; // More movement in menu
		object.transform.loc.z = height + Math.sin(Time.time() * 2.0) * heightFactor;
		object.transform.buildMatrix();

		if (!inLevel) return;

		direction.set(0,0,0,0);
		tilting.set(0,0,0,0);

		var wPos = object.transform.getWorldPosition();

		if (playable) {
			if (kb.down("w") && !tooFarF(wPos)) {
				direction.add(Vec4.yAxis());
				tilting.add(Vec4.xAxis().mult(-1));
			}
			if (kb.down("s") && !tooFarB(wPos)) {
				direction.add(Vec4.yAxis().mult(-1));
				tilting.add(Vec4.xAxis());
			}
			if (kb.down("a") && !tooFarL(wPos)) {
				direction.add(Vec4.xAxis().mult(-1));
				tilting.add(Vec4.yAxis().mult(-1));
			}
			if (kb.down("d") && !tooFarR(wPos)) {
				direction.add(Vec4.xAxis());
				tilting.add(Vec4.yAxis());
			}
		}

		var vel = rigidBody.getLinearVelocity();
		if ((tooFarF(wPos) && vel.y > 0) || (tooFarB(wPos) && vel.y < 0)) {
			vel.y *= 0.5;
			rigidBody.setLinearVelocity(vel.x, vel.y, vel.z);
		}
		if ((tooFarL(wPos) && vel.x < 0) || (tooFarR(wPos) && vel.x > 0)) {
			vel.x *= 0.5;
			rigidBody.setLinearVelocity(vel.x, vel.y, vel.z);
		}

		if (playable && direction.length() > 0) {
			Sound.interpolateVolume("UfoAccel", 0.65, 0.04);
			Sound.interpolatePitch("UfoAccel", 1.6, 0.04);

			direction.normalize().mult(SPEED * rigidBody.mass);

			if (kb.down("shift")) {
				direction.mult(RUN_FACTOR);
			}

			rigidBody.activate();
			rigidBody.applyForce(direction);
		}
		else {
			Sound.interpolateVolume("UfoAccel", 0.36, 0.02);
			Sound.interpolatePitch("UfoAccel", 1, 0.07);
		}

		tilting.mult(Helper.degToRad(TILT_ANGLE));
		object.transform.rot.lerp(object.transform.rot, new Quat().fromEuler(tilting.x, tilting.y, tilting.z), TILT_SMOOTHNESS);

		rigidBody.syncTransform();
		object.transform.buildMatrix();

		camPos.transform.loc = this.object.transform.loc.clone().add(camDist);
		camPos.transform.buildMatrix();

		if (playable && kb.started("space")) {
			beamOn = !beamOn;
		}

		if (playable) {
			if (beamOn) {
				beamCone.origin = object.transform.getWorldPosition();
				beamCone.origin.z += 6.7;
				beamCone.direction = object.transform.up().normalize().mult(-1);

				for (i in 0...Collectible.collectibles.length) {
					var col = Collectible.collectibles[i];
					var colPos = Collectible.collectiblesPositions[i];

					var dst = beamCone.pointInside(colPos);
					if (dst > 0) {
						// Non-linear falloff, only falloff between [0.7, 1]
						dst = Math.min((1 - dst) * (1 / 0.3), 1);

						if (!col.inProgress) {
							col.inProgress = true;
							col.onCollectionStarted();
						}

						// Slower beaming if the collectible is at the border
						var rb = col.rb;
						rb.activate();
						var linVel = rb.getLinearVelocity();
						if (linVel.length() < 4 || linVel.z < 0) {
							rb.applyForce(beamCone.direction.clone().mult(-BEAM_STRENGTH * rb.mass * dst));
						}
						rb.syncTransform();
						col.object.transform.buildMatrix();

						// Collected
						if (colPos.z > object.transform.loc.z - 4.5) {
							col.onCollectionFinished();

							Sound.playSound("Collected", false, 0.5);

							if (col.object.name == "ArmoryFlagPole") {
								ufo.ui.Achievements.showAchievement("Wait! What is this?");
							}
							else {
								score++;
							}
							ufo.ui.Level.setScore(score);
							col.object.remove();
							Collectible.markRemoval(i);

							if (score == Cow.amount) {
								finishLevel(true);
							}
						}
					}
					else if (col.inProgress) {
						col.inProgress = false;
						col.onCollectionStopped();
					}
				}
				Collectible.removeMarked();
			}

			var pos = beamCone.origin.clone();
			if (Observer.updateAll(beamOn, new Vec2(pos.x, pos.y))) {
				finishLevel(false);
			}

			if (Sunrise.time > Sunrise.SUNSET_TIME_END) {
				finishLevel(false);
			}
		}

		var collided = false;
		var physics = armory.trait.physics.PhysicsWorld.active;
		var contacts = physics.getContacts(rigidBody);
		if (contacts != null && contacts.length != 0) {
			collided = true;
			if (lastVelocity.length() > 10.0 && !collidedLastFrame) {
				Sound.playInScene("Collision", false, 0.5);
			}
		}
		collidedLastFrame = collided;
		lastVelocity = rigidBody.getLinearVelocity();
		lastVelocity.z = 0;
	}

	function set_beamOn(value: Bool): Bool {
		if (!playable) return true;

		beamOn = value;

		var beam = Scene.active.getMesh("LightBeam");
		var beamLight = Scene.active.getLight("LightBeamLamp");
		beam.visible = beamOn;
		beamLight.visible = beamOn;


		if (beamOn) {
			ambientLightRaw.color = beamColor;
			ambientLightRaw.strength = 35;
		}
		else {
			ambientLightRaw.color = ambientLightColor;
			ambientLightRaw.strength = ambientLightStrength;

			for (col in Collectible.collectibles) {
				if (col.inProgress) {
					col.inProgress = false;
					col.onCollectionStopped();
				}
			}
		}

		Sound.playSound("SwitchBeam", 0.4);

		return beamOn;
	}

	function finishLevel(win: Bool) {
		// Do not turn the lights off again
		Observer.stopUpdate = true;
		Sunrise.stop();
		Cow.amount = 0;

		playable = false;
		rigidBody.setLinearVelocity(0, 0, 0);

		Sound.channels["MusicLevel"].stop();

		if (win) {
			Sound.playInScene("Success", 0.5);
			ufo.ui.Level.showWin();
		}
		else {
			Sound.playInScene("Failure", 0.5);
			ufo.ui.Level.showGameOver();
		}
	}

	// Check playable bounds
	inline function tooFarF(pos: Vec4): Bool { return pos.y > boundsFBLR.x; }
	inline function tooFarB(pos: Vec4): Bool { return pos.y < boundsFBLR.y; }
	inline function tooFarL(pos: Vec4): Bool { return pos.x < boundsFBLR.z; }
	inline function tooFarR(pos: Vec4): Bool { return pos.x > boundsFBLR.w; }
}
