package ufo;

import kha.arrays.Float32Array;

import iron.Scene;
import iron.data.SceneFormat;
import iron.object.LightObject;
import iron.object.ObjectAnimation;

class Sunrise extends iron.Trait {
	public static inline var SUNSET_TIME_BEGIN = 5.0;
	public static inline var SUNSET_TIME_END = 6.0;

	static inline var MINUTE: Float = 1 / 60;

	public static var time: Float = 0.0; // hours. 0.0 = midnight
	public static var stopUpdate = false;

	static var startedSunrise = false;

	var lightDataRaw: TLightData;
	static var origIrradiance: Null<Float32Array>;

	var anim: ObjectAnimation;

	public function new() {
		super();

		notifyOnInit(function() {
			// Reset when playing again
			time = 0.0;
			stopUpdate = false;
			startedSunrise = false;

			// Make irradiance copy if the scene is played for the first time,
			// else reset to the original irradiance
			var irr = Scene.active.world.probe.irradiance;
			if (origIrradiance == null) {
				origIrradiance = new Float32Array(irr.length);
				copyFloat32Array(irr, origIrradiance);
			} else {
				copyFloat32Array(origIrradiance, irr);
			}

			lightDataRaw = cast(object, LightObject).data.raw;
			lightDataRaw.strength = 0.0;
			lightDataRaw.shadows_bias = 1.0;// Seems to be 0, no idea why...

			anim = cast object.animation;
			anim.play("SunriseAction", null, 0.0, 1.0, false); // Set speed to 0, update manually
		});

		notifyOnUpdate(function() {
			if (stopUpdate) return;

			time += MINUTE / (60 / 3); // every second is 3 minutes ingame

			ufo.ui.Level.setTime(time);

			if (time > SUNSET_TIME_BEGIN) {
				if (!startedSunrise) {
					startedSunrise = true;
					Sound.playInScene("clock", true, 0.2);
					Sound.playInScene("MorningBirds", true, 0.0);
				}

				var progress = time - SUNSET_TIME_BEGIN;
				anim.update(progress * anim.totalFrames());
				lightDataRaw.strength = progress;

				Sound.channels["clock"].volume = 0.2 + 0.3 * progress;
				Sound.channels["MorningBirds"].volume = 0.8 * progress;

				var irr = Scene.active.world.probe.irradiance;
				for (i in 0...irr.length) {
					irr[i] = origIrradiance[i] - 0.1 * progress;
				}
			}
		});
	}

	public static function stop() {
		stopUpdate = true;
		if (startedSunrise) {
			Sound.channels["clock"].stop();
		}
	}

	public static function copyFloat32Array(from: Float32Array, to: Float32Array) {
		for (i in 0...from.length) {
			to[i] = from[i];
		}
	}
}
