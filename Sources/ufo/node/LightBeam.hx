package ufo.node;

@:access(armory.logicnode.LogicNode)@:keep class LightBeam extends armory.logicnode.LogicTree {

	var functionNodes:Map<String, armory.logicnode.FunctionNode>;

	var functionOutputNodes:Map<String, armory.logicnode.FunctionOutputNode>;

	public function new() {
		super();
		this.functionNodes = new Map();
		this.functionOutputNodes = new Map();
		notifyOnAdd(add);
	}

	override public function add() {
		var _OnUpdate_001 = new armory.logicnode.OnUpdateNode(this);
		_OnUpdate_001.property0 = "Update";
		_OnUpdate_001.preallocInputs(0);
		_OnUpdate_001.preallocOutputs(1);
		armory.logicnode.LogicNode.addLink(_OnUpdate_001, new armory.logicnode.NullNode(this), 0, 0);
		var _RotateObject_001 = new armory.logicnode.RotateObjectNode(this);
		_RotateObject_001.property0 = "Angle Axies (Degrees)";
		_RotateObject_001.preallocInputs(4);
		_RotateObject_001.preallocOutputs(1);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.NullNode(this), _RotateObject_001, 0, 0);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.ObjectNode(this, ""), _RotateObject_001, 0, 1);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.VectorNode(this, 1.0,0.0,0.0), _RotateObject_001, 0, 2);
		var _Math = new armory.logicnode.MathNode(this);
		_Math.property0 = "Multiply";
		_Math.property1 = false;
		_Math.preallocInputs(2);
		_Math.preallocOutputs(1);
		var _Math_002 = new armory.logicnode.MathNode(this);
		_Math_002.property0 = "Sine";
		_Math_002.property1 = false;
		_Math_002.preallocInputs(1);
		_Math_002.preallocOutputs(1);
		var _Math_003 = new armory.logicnode.MathNode(this);
		_Math_003.property0 = "Multiply";
		_Math_003.property1 = false;
		_Math_003.preallocInputs(2);
		_Math_003.preallocOutputs(1);
		var _GetApplicationTime_001 = new armory.logicnode.TimeNode(this);
		_GetApplicationTime_001.preallocInputs(0);
		_GetApplicationTime_001.preallocOutputs(2);
		armory.logicnode.LogicNode.addLink(_GetApplicationTime_001, new armory.logicnode.FloatNode(this, 0.0), 1, 0);
		armory.logicnode.LogicNode.addLink(_GetApplicationTime_001, _Math_003, 0, 0);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.FloatNode(this, 0.029999999329447746), _Math_003, 0, 1);
		armory.logicnode.LogicNode.addLink(_Math_003, _Math_002, 0, 0);
		armory.logicnode.LogicNode.addLink(_Math_002, _Math, 0, 0);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.FloatNode(this, 20.0), _Math, 0, 1);
		armory.logicnode.LogicNode.addLink(_Math, _RotateObject_001, 0, 3);
		armory.logicnode.LogicNode.addLink(_RotateObject_001, new armory.logicnode.NullNode(this), 0, 0);
		var _RotateObject = new armory.logicnode.RotateObjectNode(this);
		_RotateObject.property0 = "Local";
		_RotateObject.preallocInputs(3);
		_RotateObject.preallocOutputs(1);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.NullNode(this), _RotateObject, 0, 0);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.ObjectNode(this, ""), _RotateObject, 0, 1);
		var _Rotation = new armory.logicnode.RotationNode(this);
		_Rotation.property0 = "EulerAngles";
		_Rotation.property1 = "Rad";
		_Rotation.property2 = "XYZ";
		_Rotation.preallocInputs(2);
		_Rotation.preallocOutputs(1);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.VectorNode(this, 0.0,0.0,0.0), _Rotation, 0, 0);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.FloatNode(this, 0.0), _Rotation, 0, 1);
		armory.logicnode.LogicNode.addLink(_Rotation, _RotateObject, 0, 2);
		armory.logicnode.LogicNode.addLink(_RotateObject, new armory.logicnode.NullNode(this), 0, 0);
	}
}