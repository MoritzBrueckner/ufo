package ufo;

import kha.FastFloat;
import kha.math.Random;

import iron.math.Vec4;

import armory.trait.physics.bullet.RigidBody;

using armory.object.TransformExtension;

class Windmill extends iron.Trait {
	static inline var MAX_WIND_STRENGTH: FastFloat = 0.3;
	static inline var CHANGE_TIME_MIN = 20;
	static inline var CHANGE_TIME_MAX = 120;

	var nextStrengthChange = 0;
	var currentWindStrength: FastFloat = 0.0;

	var rand = new Random(812111199);
	var rb: RigidBody;

	public function new() {
		super();

		notifyOnInit(init);
		notifyOnUpdate(update);
	}

	function init() {
		rb = this.object.getTrait(RigidBody);
		rb.angularDamping = 0.9;
		rb.disableGravity();
	}

	function update() {
		if (nextStrengthChange == 0 || currentWindStrength > MAX_WIND_STRENGTH) {
			nextStrengthChange = rand.GetIn(CHANGE_TIME_MIN, CHANGE_TIME_MAX);
			currentWindStrength = rand.GetFloatIn(-MAX_WIND_STRENGTH, MAX_WIND_STRENGTH);
		}

		var ufo = ufo.Ufo.instance;
		if (ufo.beamOn) {
			var ufoPos = ufo.beamCone.origin.clone();
			ufoPos.z = object.transform.loc.z;

			var dst = ufoPos.distanceTo(object.transform.loc);

			if (dst < 6) {
				currentWindStrength = 1.3;
			}
		}

		rb.activate();
		rb.applyTorque(object.transform.worldVecToOrientation(new Vec4(currentWindStrength, 0.0, 0.0, 0.0)));
		rb.syncTransform();
		object.transform.buildMatrix();

		nextStrengthChange--;
	}
}
