package ufo;

import armory.trait.physics.PhysicsWorld;
import iron.math.Vec4;
import iron.object.CameraObject;
import iron.object.Object;
import iron.Scene;
import iron.system.Input;

using armory.object.TransformExtension;


class Camera extends iron.Trait {
	var smoothness = 0.1;

	var target: Object;

	var mouse = Input.getMouse();

	public function new() {
		super();

		notifyOnInit(this.onInit);
		notifyOnUpdate(this.onUpdate);
	}

	function onInit() {
		this.target = Scene.active.getChild("CamPos");
	}

	function onUpdate() {
		// Slow parent
		var dist = target.transform.getWorldPosition().sub(object.transform.loc);
		// trace(target.transform.loc, object.transform.loc, dist.length());
		if (dist.length() > 0.05) {
			object.transform.loc.add(dist.mult(smoothness));
		}

		object.transform.buildMatrix();
	}
}
