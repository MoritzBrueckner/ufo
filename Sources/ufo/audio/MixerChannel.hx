class MixerChannel {
	public var initialVolume(default, null): kha.FastFloat;

	var _channel: UFO_ResamplingAudioChannel;

	public function new(channel: UFO_ResamplingAudioChannel) {
		this._channel = channel;
		this.initialVolume = channel.volume;
	}
}
