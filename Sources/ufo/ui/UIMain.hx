package ufo.ui;

import iron.Scene;
import iron.math.Vec4;

import armory.trait.physics.PhysicsWorld;

import koui.Koui;
import koui.utils.SceneManager;
import koui.elements.*;
import koui.elements.layouts.*;
import koui.elements.layouts.Layout.Anchor;
import koui.events.Events.Event;


class UIMain extends iron.Trait {
	static var initialized = false;
	static var blendPanel: Panel;

	public function new() {
		super();

		notifyOnInit(() -> {
			if (!initialized) {
				kha.Assets.loadEverything(() -> {
					// Make sure we can start the game in any scene for debugging
					Koui.init(() -> {
						SceneManager.addScene("Intro", Intro.setup);
						SceneManager.addScene("MainMenu", MainMenu.setup);
						SceneManager.addScene("Level", Level.setup);
						SceneManager.addScene("HowTo", HowTo.setup);

						SceneManager.setScene(iron.Scene.active.raw.name);

						blendPanel = new Panel();
						blendPanel.visible = false;
						blendPanel.setTID("blendpanel");
						Koui.add(blendPanel);

						// Fix not implemented resize callback on Krom
						#if kha_krom
						iron.App.onResize = () -> {
							@:privateAccess Koui.onResize(iron.App.w(), iron.App.h());
						};
						#end

						Achievements.init();

						initialized = true;
					});
				});
			}
		});

		notifyOnUpdate(function() {
			if (!initialized) return;

			switch (Scene.active.raw.name) {
				case "Intro": Intro.update();
				case "MainMenu": MainMenu.update();
				case "Level": Level.update();
				case "HowTo": HowTo.update();
				default:
			}

			Achievements.update();
		});
	}

	public static inline function setScene(name: String) {
		Scene.setActive(name, (o: iron.object.Object) -> {
			// Ugly workaround to prevent the Ufo from sinking in the main menu
			// in the published build
			if (Scene.active.raw.name == "Level") {
				PhysicsWorld.active.setGravity(new Vec4(0.0, 0.0, -9.81, 0.0));

			} else {
				PhysicsWorld.active.setGravity(new Vec4());
			}

			ufo.Observer.reset();
			ufo.Collectible.reset();

			ufo.Sound.clearScene();
			SceneManager.setScene(name);
		});
	}

	public static function showDialog(title: String, text: String, btnText: String, action: Void->Void): AnchorPane {
		var layout = SceneManager.activeScene;

		var dialogLayout = new AnchorPane(0, 0, 1000, 400);
		dialogLayout.setTID("lyt_fixed");

		var panelLayout = new AnchorPane(0, 0, 1000, 400);
		panelLayout.setPadding(30, 30, 30, 30);

		var labelGameOver = new Label(title);
		labelGameOver.posX = -10;
		labelGameOver.setTID("label_title");
		var labelSub = new Label(text);
		labelSub.posY = 120;

		var btnMenu = makeButton(btnText, () -> {
			action();
			layout.remove(dialogLayout);
		});

		layout.add(dialogLayout, MiddleCenter);

		dialogLayout.add(new Panel(), MiddleCenter);
		dialogLayout.add(panelLayout, MiddleCenter);

		panelLayout.add(labelGameOver);
		panelLayout.add(labelSub);
		panelLayout.add(btnMenu, BottomCenter);

		return dialogLayout;
	}

	public static function setBlendOpacity(value: kha.FastFloat) {
		// Also use visibility to auto block inputs below the pane
		if (value == 0) {
			blendPanel.visible = false;
		}
		else {
			blendPanel.visible = true;
			@:privateAccess blendPanel.style.opacity = value;
		}
	}

	public static function makeButton(text: String, onClick: Void -> Void) {
		var btn = new Button(text);

		btn.onMouseHover((e: Event) -> {
			switch (e.state) {
				case Activated:
					Sound.playSound("click2", 0.4);
				case Deactivated:
					Sound.playSound("click3", 0.4);
				default:
			}
		});

		btn.onMouseClick((e: Event) -> {
			if (e.mouseButton == Left) {
				switch (e.state) {
					case Activated:
						Sound.playSound("click1", 0.4);
					case Deactivated:
						onClick();
					default:
				}
			}
		});

		return btn;
	}
}
