package ufo.ui;

import iron.system.Tween;

import armory.math.Helper;

import koui.Koui;
import koui.elements.*;
import koui.elements.layouts.*;
import koui.elements.layouts.Layout.Anchor;
import koui.utils.Set;


class Achievements {
	static inline var DURATION = 3 * 60;

	static var achievementPane: AnchorPane;
	static var achievementText: Label;

	static var state: State = None;
	static var timer = 0;

	static var collected: Set<String> = new Set();

	public static function init() {
		achievementPane = new AnchorPane(0, 80, 300, 80);
		achievementPane.setTID("achievement_pane");
		achievementPane.visible = false;
		achievementPane.posX = achievementPane.width; // Prevent flickering when blending in

		var achievementPanel = new Panel();

		var achievementTitle = new Label("Achievement unlocked");
		achievementTitle.setTID("label_paragraph");
		achievementText = new Label("");
		achievementText.setTID("label_small");

		// Fake padding to use one nested layout less
		achievementTitle.posX = 8;
		achievementTitle.posY = 8;
		achievementText.posX = 8;

		achievementPane.add(achievementPanel);
		achievementPane.add(achievementTitle, TopLeft);
		achievementPane.add(achievementText, MiddleLeft);

		Koui.add(achievementPane, TopRight);
	}

	public static function update() {
		var posFactor = 0.0;
		switch (state) {
			case BlendIn:
				posFactor = Tween.easeQuadInOut(timer / 60);
				if (timer == 60) {
					setState(Show);
				}

			case Show:
				posFactor = 1.0;
				if (timer == DURATION) {
					setState(BlendOut);
				}

			case BlendOut:
				posFactor = 1 - Tween.easeQuadInOut(timer / 60);
				if (timer == 60) {
					setState(None);
					achievementPane.visible = false;
				}

			case None:
				return;
		}

		achievementPane.posX = Std.int((1 - posFactor) * achievementPane.width);

		timer++;
	}

	public static inline function showAchievement(text: String) {
		if (collected.has(text)) return;

		collected.add(text);
		setState(BlendIn);
		achievementText.text = text;
		achievementPane.visible = true;
		ufo.Sound.playSound("Achievement", false, 0.4);
	}

	static inline function setState(state: State) {
		Achievements.state = state;
		timer = 0;
	}
}

enum abstract State(Int) {
	var BlendIn;
	var Show;
	var BlendOut;
	var None;
}
