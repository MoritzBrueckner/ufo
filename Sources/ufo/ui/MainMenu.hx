package ufo.ui;

import koui.elements.*;
import koui.elements.layouts.*;
import koui.elements.layouts.Layout.Anchor;
import koui.events.Events.Event;
import koui.events.Events.EventType;

import ufo.Sound;

class MainMenu {
	static var initialized = false;

	static var aboutLayout: AnchorPane;

	static var thanks: Array<Array<Null<String>>> = [
		["The Armory3D community!", null],
		["eracoon: Suburb Assets pt1", "https://opengameart.org/content/suburb-asset-pt1"],
		["Kenney: Nature Kit", "https://www.kenney.nl/assets/nature-kit"],
		["vertexcat: Farm Animals Set", "https://vertexcat.itch.io/farm-animals-set"],
		["sigma7zero: LiquidFX", "https://github.com/sigma7zero/LiquidFX"],
		["Sonniss GDC Audio Bundles 2016-2020", "https://sonniss.com/gameaudiogdc"],
		["Apostrophic Lab: Komika Display Font", "https://www.fontsquirrel.com/fonts/Komika-Display"],
		["ShyFonts: Speakeasy Font", "https://www.fontsquirrel.com/fonts/SF-Speakeasy"],
	];

	public static function setup(layout: AnchorPane) {
		layout.setPadding(200, 200, 140, 140);

		var buttonBar = new Expander(0, 0, 400, 400, DOWN);
		buttonBar.spacing = 52;

		var logo = new ImagePanel(kha.Assets.images.logo);

		var btnPlay = UIMain.makeButton("Play", () -> {
			HowTo.timer = 0;
			UIMain.setScene("HowTo");
		});

		var btnAbout = UIMain.makeButton("About", () -> {
			aboutLayout.visible = !aboutLayout.visible;
		});

		var btnQuit = UIMain.makeButton("Quit Game", () -> {
			kha.System.stop();
		});

		buttonBar.add(logo);
		buttonBar.add(btnPlay);
		buttonBar.add(btnAbout);
		buttonBar.add(btnQuit);

		layout.add(buttonBar, Anchor.TopLeft);

		setupAboutPage(layout);

		initialized = true;
	}

	static function setupPage(layout: AnchorPane, title: String): Array<Layout> {
		var positioner = new AnchorPane(400 + layout.paddingLeft, 200, 0, 0);
		var panel = new Panel();
		var panelLayout = new AnchorPane(0,0,0,0);
		panelLayout.setPadding(20, 20, 20, 20);

		var pageTitle = new Label(title);

		var expander = new Expander(0, 60, 200, 0, DOWN);
		expander.setTID("lyt_scale_y");
		expander.spacing = 25;

		positioner.add(panel, Anchor.TopCenter);
		positioner.add(panelLayout, Anchor.TopCenter);

		panelLayout.add(pageTitle);
		panelLayout.add(expander);

		layout.add(positioner);
		positioner.visible = false;

		return [positioner, expander];
	}

	static function setupAboutPage(layout: AnchorPane) {
		var lyts = setupPage(layout, "About");
		aboutLayout = cast lyts[0];
		var expander: Expander = cast lyts[1];

		var description = new Label("This game was made by timodriaan in March/April 2021 for the ");
		description.setTID("label_paragraph");
		var itchLink = createLink("Armory3D Community Game Jam.", "https://itch.io/jam/armory-3d-community-game-jam");

		// Workaround as long as there is no rich text support in Koui
		var descriptionBox = new Expander(0,0,0, @:privateAccess description.style.font.size, RIGHT);
		descriptionBox.setTID("lyt_fixed_y");

		var thanksTo = new Label("Thanks to");
		thanksTo.setTID("label_subtitle");

		var thanksBox = new RowLayout(0, 0, 0, 28 * thanks.length, thanks.length);
		descriptionBox.setTID("lyt_fixed_y");

		var r = 0;
		for (t in thanks) {
			var labelT: Element;

			var entry = '- ' + t[0];

			// Has link
			if (t[1] != null) {
				labelT = createLink(entry, t[1]);
			}

			// Has no link
			else {
				labelT = new Label(entry);
				labelT.setTID("label_paragraph");
			}

			thanksBox.addToRow(labelT, r);
			r++;
		}

		var madeWith = new Label("Made with");
		madeWith.setTID("label_subtitle");

		var imgArmory = createLogo("ArmoryBanner");
		var imgKoui = createLogo("KouiBanner");
		var imgBlender = createLogo("BlenderLogo");

		var madeWithBox = new Expander(0, 0, 0, 64, RIGHT);
		madeWithBox.spacing = 28;

		madeWithBox.add(imgArmory);
		madeWithBox.add(imgKoui);
		madeWithBox.add(imgBlender);

		expander.add(descriptionBox);
		expander.add(thanksTo);
		expander.add(thanksBox);
		expander.add(madeWith);
		expander.add(madeWithBox);

		descriptionBox.add(description);
		descriptionBox.add(itchLink);
	}

	@:access(koui.elements.Element)
	public static function update() {
		if (!initialized) return;

		// HowTo scene fades the volume out
		Sound.interpolateVolume("MainMenu", 0.5, 0.05);
	}

	static function createLogo(imgName: String): ImagePanel {
		var img = kha.Assets.images.get(imgName);
		// img.generateMipmaps(1000);
		var ratio = img.realWidth / img.realHeight;

		var panel = new ImagePanel(img);
		panel.setScaleQuality(High);
		panel.setScale(true, Std.int(64 * ratio), 64);

		return panel;
	}

	static function createLink(text: String, url: String): Label {
		var link = new Label(text);
		link.setTID("label_url");
		link.eventMask = EventType.MOUSE_TYPES;

		link.onMouseClick((e: Event) -> {
			if (e.state == Deactivated && e.mouseButton == Left) {
				#if kha_krom
					switch (kha.System.systemId) {
						case "Windows": Krom.sysCommand('explorer "' + url + '"');
						case "Linux": Krom.sysCommand('xdg-open "' + url + '"');
						case "Mac": Krom.sysCommand('open "' + url + '"');
						default:
					}
				#else
					kha.System.loadUrl(url);
				#end
			}
		});

		return link;
	}
}
