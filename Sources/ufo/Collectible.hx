package ufo;

import iron.math.Vec4;
import iron.object.Object;

import armory.trait.physics.bullet.RigidBody;

using armory.object.TransformExtension;

class Collectible extends iron.Trait {
	public static var collectibles: Array<Collectible> = new Array();
	// Data-oriented approach
	public static var collectiblesPositions: Array<Vec4> = new Array();
	static var removals: Array<Int> = new Array();

	public var inProgress = false;
	public var rb(default, null): RigidBody;

	public function new() {
		super();

		notifyOnInit(function() {
			collectibles.push(this);
			collectiblesPositions.push(object.transform.loc);

			rb = this.object.getTrait(RigidBody);
		});
	}

	public static inline function markRemoval(i: Int) {
		removals.push(i);
	}

	public static inline function removeMarked() {
		var i = removals.length;
		if (i == 0) return;
		// Sort and iterate backwards to prevent index shifting when removing
		removals.sort((a: Int, b: Int) -> { return a == b ? 0 : (a > b ? 1 : -1); });
		while (i > 0) {
			collectibles.splice(removals[i - 1], 1);
			collectiblesPositions.splice(removals[i - 1], 1);
			i--;
		}
		removals.resize(0);
	}

	public static function reset() {
		collectibles.resize(0);
		collectiblesPositions.resize(0);
		removals.resize(0);
	}

	public function onCollectionStarted() {}
	public function onCollectionFinished() {}
	public function onCollectionStopped() {}
}
