// Auto-generated
package ;
class Main {
    public static inline var projectName = 'The Grand Cattle Heist';
    public static inline var projectVersion = '1.0.30';
    public static inline var projectPackage = 'ufo';
    public static function main() {
        iron.object.BoneAnimation.skinMaxBones = 22;
            iron.object.LightObject.cascadeCount = 4;
            iron.object.LightObject.cascadeSplitFactor = 0.800000011920929;
        armory.system.Starter.main(
            'MainMenu',
            0,
            true,
            true,
            true,
            1920,
            1080,
            1,
            true,
            armory.renderpath.RenderPathCreator.get
        );
    }
}
