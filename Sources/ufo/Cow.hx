package ufo;

import kha.math.Random;

import iron.object.BoneAnimation;

class Cow extends Collectible {
	var armature: BoneAnimation;

	public static var amount = 0;

	var random: Random;

	public function new() {
		super();

		notifyOnInit(() -> {
			armature = cast object.children[0].children[0].animation;

			random = new Random(object.uid);
			amount++;
			ufo.ui.Level.setScore(0);

			chooseNextAction();
		});
	}

	override public function onCollectionStarted() {
		armature.play("CowRun", null, 0.3, 1.4, true);
	}

	function chooseNextAction() {
		if (inProgress) return;

		if (random.GetUpTo(5) == 0) {
			armature.play("CowEat", chooseNextAction, 0.1, 1.0, false);
		}
		else {
			armature.play("CowIdle", chooseNextAction, 0.1, 1.0, false);
		}
	}

	override public function onCollectionStopped() {
		chooseNextAction();
	}
}
